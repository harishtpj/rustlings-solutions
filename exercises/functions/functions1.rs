// functions1.rs
//
// Execute `rustlings hint functions1` or use the `hint` watch subcommand for a
// hint.

fn call_me() {
    println!("Call works");
}

fn main() {
    call_me();
}
